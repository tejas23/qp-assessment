package com.assisment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assisment.dto.OrderRequest;
import com.assisment.pojo.Order;
import com.assisment.repo.OrderRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepo orderRepo;

	@Override
	public void createOrder(OrderRequest orderRequest) {
		
		int userid = orderRequest.getUserId();

		for (int itemId : orderRequest.getItemIds()) {
			Order order = new Order();
			order.setUserid(userid);
			order.setGrocery_item_id(itemId);
			orderRepo.save(order).getId();
		}
	}

}
