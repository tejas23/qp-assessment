package com.assisment.service;

import java.util.List;

import com.assisment.pojo.GroceryItems;
import com.assisment.repo.GroceryItemRepo;

public interface GroceryService {
	
	void addGroceryItem(GroceryItems groceryItem);
    List<GroceryItems> getAllGroceryItems();
    void removeGroceryItem(Long itemId);
    void updateGroceryItem(Long itemId, GroceryItems groceryItem);
    void manageInventory(Long itemId, int quantity);
}
