package com.assisment.service;

import com.assisment.dto.OrderRequest;

public interface OrderService {
	
	public void createOrder(OrderRequest orderRequest);
}
