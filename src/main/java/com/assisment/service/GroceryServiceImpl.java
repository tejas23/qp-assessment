package com.assisment.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assisment.pojo.GroceryItems;
import com.assisment.repo.GroceryItemRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class GroceryServiceImpl implements GroceryService {

	@Autowired
	private GroceryItemRepo groceryRepo;

	@Override
	public void removeGroceryItem(Long itemId) {
		groceryRepo.deleteById(itemId);
	}

	@Override
	public void addGroceryItem(GroceryItems groceryItem) {
		groceryRepo.save(groceryItem);
	}

	@Override
	public List<GroceryItems> getAllGroceryItems() {
		List<GroceryItems> list = groceryRepo.findAll();
		return list;
	}

	@Override
	public void updateGroceryItem(Long itemId, GroceryItems groceryItem) {
		Optional<GroceryItems> existingItem = groceryRepo.findById(itemId);
		if (existingItem.isEmpty()) {
			throw new RuntimeException("Item id is not present");
		} else {
			GroceryItems currentData = existingItem.get();
			currentData.setName(groceryItem.getName());
			currentData.setPrice(groceryItem.getPrice());
			currentData.setQuantityAvailable(groceryItem.getQuantityAvailable());
			groceryRepo.save(currentData);
		}
	}

	@Override
	public void manageInventory(Long itemId, int quantity) {
		Optional<GroceryItems> existingItem = groceryRepo.findById(itemId);
		if (existingItem.isEmpty()) {
			throw new RuntimeException("Item id is not present");
		} else {
			GroceryItems currentData = existingItem.get();
			int newQuantity = currentData.getQuantityAvailable() + quantity;
			currentData.setQuantityAvailable(newQuantity);
			groceryRepo.save(currentData);
		}

	}

}
