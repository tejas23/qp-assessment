package com.assisment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QrAssismentApplication {

	public static void main(String[] args) {
		SpringApplication.run(QrAssismentApplication.class, args);
	}

}
