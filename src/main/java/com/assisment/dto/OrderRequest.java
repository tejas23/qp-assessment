package com.assisment.dto;

import java.util.List;

public class OrderRequest {

	private int userId;
    private List<Integer> itemIds;
    
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public List<Integer> getItemIds() {
		return itemIds;
	}
	public void setItemIds(List<Integer> itemIds) {
		this.itemIds = itemIds;
	}
    
    
}
