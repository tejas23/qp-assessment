package com.assisment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assisment.dto.OrderRequest;
import com.assisment.pojo.GroceryItems;
import com.assisment.service.GroceryServiceImpl;
import com.assisment.service.OrderServiceImpl;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private GroceryServiceImpl groceryService;
	
	@Autowired
	private OrderServiceImpl orderService;
	
	@GetMapping("/viewAll")
	public ResponseEntity<?> viewAllGroceryItems(){
		try {
			List<GroceryItems> list = groceryService.getAllGroceryItems();
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/order")
	public ResponseEntity<?> orderItems(@RequestBody OrderRequest orderRequest){
		try {
			orderService.createOrder(orderRequest);
			return new ResponseEntity<>("Ordered Successfully", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Order Failed", HttpStatus.INTERNAL_SERVER_ERROR);
		}	
	}
	
	
	
}
