package com.assisment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.assisment.pojo.GroceryItems;
import com.assisment.service.GroceryService;
import com.assisment.service.GroceryServiceImpl;

@RestController
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	private GroceryServiceImpl groceryService;

	@PostMapping("/add")
	public ResponseEntity<?> addGrocery(@RequestBody GroceryItems groceryItems) {
		try {
			groceryService.addGroceryItem(groceryItems);
			return new ResponseEntity<>("Grocery Item has been added", HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>("Failed to add the item", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@GetMapping("/view")
	public ResponseEntity<?> viewGrocery() {
		try {
			List<GroceryItems> list = groceryService.getAllGroceryItems();
			return new ResponseEntity<>(list, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Something went wrong", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@DeleteMapping("/remove/{id}")
	public ResponseEntity<?> deleteItem(@PathVariable Long id) {
		try {
			groceryService.removeGroceryItem(id);
			return new ResponseEntity<>("Item Deleted", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Failed to remove", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/update/{itemId}")
	public ResponseEntity<?> updateItems(@PathVariable Long itemId, @RequestBody GroceryItems groceryItem) {
		try {
			groceryService.updateGroceryItem(itemId, groceryItem);
			return new ResponseEntity<>("Item Updated", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Failed to update", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@PutMapping("/manage/{itemId}")
	public ResponseEntity<?> manageItems(@PathVariable Long itemId, @RequestParam int quantity) {
		try {
			groceryService.manageInventory(itemId, quantity);
			return new ResponseEntity<>("Item Inventry Updated", HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("Failed to Update Inventory", HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
