package com.assisment.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assisment.pojo.Order;

@Repository
public interface OrderRepo extends JpaRepository<Order, Integer>{

}
