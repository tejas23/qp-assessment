package com.assisment.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assisment.pojo.GroceryItems;

@Repository
public interface GroceryItemRepo extends JpaRepository<GroceryItems, Long>{

}
