# Grocery Store Management System

This project aims to provide a simple yet effective database schema for managing a grocery store. The schema includes tables for users, authorities, orders, and grocery items.

## Users Table

The `users` table stores information about users registered in the system. It includes fields for username, hashed password, and account status.

### Fields

- `username`: VARCHAR(50) - User's username
- `password`: VARCHAR(120) - Hashed password
- `enabled`: TINYINT(1) - Account enabled status

Example data:
```sql

CREATE TABLE `users` (
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(120) NOT NULL,
  `enabled` TINYINT(1) NOT NULL,
  PRIMARY KEY (`username`)
);

insert into users values ('smith', '$2a$12$hhdXrq63gHFVkL2G1jwDuOBcrNEjX7mwZHUXEgQGwL18v6CD1zkra',  1);
insert into users values ('tejas', '$2a$12$u/yT8eQvJzcOpBhRw7BxB.cBvPgK.h3icM/8LejNTzg7cd0SnW.tW',  1);
insert into users values ('aditya', '$2a$12$dDSpkqieZa3bl1Rh92y0GunlSvgjIMxwOSYfFF2xIOVd8mHnFj2JG',  1);
```

## Authorities Table

The `authorities` table associates users with their respective roles or authorities.

### Fields

- `username`: VARCHAR(50) - User's username
- `authority`: VARCHAR(50) - User's role or authority

Example data:
```sql

CREATE TABLE `authorities` (
  `username` VARCHAR(50) NOT NULL,
  `authority` VARCHAR(50) NOT NULL,
  KEY `username` (`username`),
  CONSTRAINT `authorities_ibfk_1` FOREIGN KEY (`username`)
  REFERENCES `users` (`username`)
);

insert into authorities values ('aditya', 'ROLE_ADMIN');
insert into authorities values ('smith', 'ROLE_USER');
insert into authorities values ('tejas', 'ROLE_USER');
```

## Orders Table

The `orders` table records orders made by users.

### Fields

- `id`: INT - Order ID (auto-incremented)
- `userid`: INT - ID of the user making the order
- `grocery_item_id`: INT - ID of the grocery item ordered

Schemas
```sql
CREATE TABLE orders(
	id INT AUTO_INCREMENT PRIMARY KEY, 
	userid INT,
	grocery_item_id INT
);
```

## Grocery Item Table

The `grocery_item` table contains information about available grocery items.

### Fields

- `id`: INT - Grocery item ID
- `name`: VARCHAR(255) - Name of the grocery item
- `price`: DOUBLE - Price of the item
- `quantity_available`: INT - Quantity available

Schemas
```sql
CREATE TABLE grocery_item (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    price DOUBLE NOT NULL,
    quantity_available INT NOT NULL
);
```

